# Diploma task

Create a simple Web-application (see the description in the “Application” section below), CI/CD infrastructure and pipeline for it.
## Acceptance Criteria and presentation
A short presentation (.ppt or other) which contains description of the solution should be prepared and sent to the commission before a demo session.

The working application with the pipeline is to be demonstrated live on a “protection of the diploma” session for experts with comments and explanation of the details of the implementation, reasons of choosing tools and technologies.

Detailed requirements/criteria:

|**Criteria**|**Reqiurements**|**Related Module**|
| :-: | :-: | :-: |
|**SCM**|Application sources should be placed in Git repository. Branching strategy should be explained.|Git|
|**Tests\***|CI pipeline may contain unit tests, smoke tests, linter check. |CI/CD|
|**Quality gate**|CI/CD pipeline should use some quality/vulnerability control tool like a Sonar or Anchore.|CI/CD|
|**IaC**|CI/CI and runtime infrastructure should be described as a code using Terraform, CloudFormation, or any similar tool. On the demonstration deployment procedure should be shown.|Cloud, Terraform, Ansible|
|**Orchestration**|All non cloud-native tools should be spinned up inside a K8S/OpenShift cluster inside a cloud. Application runtime environments should be inside the cluster too.|Kubernetes|
|**Logging**|Infrastructure should have centralized log collection/display system. Logs of the application components and infra components should be collected.|Monitoring and Logging|
|**Monitoring**|Infrastructure should have centralized metric collection/display system. Metrics of the application components and infra components should be collected.|<p>Monitoring and Logging</p><p></p>|
|**Runtime/Deployment**|Runtime infrastructure should have production and non production environments.  Deploy/release strategy should be explained.|CI/CD|
|**Scalability/redundancy**|Scalability should be provided and demonstrated|Kubernetes|
|**Cloud and Cost efficiency\*\***|Cloud resources and services must be used for the task. Report about the Cloud resource usage and the cost must be provided in the presentation. It should be efficient (minimal) – in accordance to the solving tasks. You can choose any cloud provider taking into account possible extra costs for the resources. |Cloud|
*\* Nice to have – optional*

*\*\* Be careful with the Cloud resource usage and check the costs for not to exceed limits! Switch off your machines when you are not using them!*
## Application
Develop a simple (lightweight) 3-tire application (front-end, back-end, database).

Back-end (collects data) must:

1. Retrieve a portion of data from API (see in your Variant) and store it in a database

2. Update data on demand

3. Update DB schema if needed on app’s update

Front-end (outputs data) must:

1. Display any portion of the data stored in the DB

2. Provide a method to trigger data update process

Database:

1. Choose Database type and data scheme in a suitable manner. 

2. Data must be stored in a persistent way

3. It’s better to use cloud native DB solutions like an RDS/AzureSQL/CloudSQL.

**You’ll get your Variant of the application individually.**

**Variant 7**. Using API https://www.metaweather.com/api/ get data about weather in Moscow for current month and store it into your DB: 
- id, 
- weather_state_name, 
- wind_direction_compass, 
- created, 
- applicable_date, 
- min_temp, 
- max_temp, 
- the_temp. 

Output the data by date (the date is set) in form of a table and sort them by created in ascending order.

# Solution

IAM user rights required to start a project in the AWS cloud:
1. VPC,
2. RDS,
3. EC2,
4. EX,
5. ELB,
6. ROUTE53.

To initialize the project, you need the following:
1. Prepare your dns name for the project.
2. Prepare AWS CLI on your workstation(AWS_ACCESS_KEY, AWS_SECRET_ACCESS_KEY, information at the link: https://aws.amazon.com/ru/blogs/security/wheres-my-secret-access-key /)
3. Run the script **init_terraform_aws.sh** to get a part of the variables for gitlab ci and prepare the cloud infrastructure for the application deployment. And fill the gitlab ci variables with the received values.
4. Run the script **init_k8s_var_for_gitlab.sh** to get the remaining variables for gitlab ci and prepare kubernetes infrastructure for application deployment. And fill the gitlab ci variables with the received values.

Everything is ready! Here are the variables that are used in this project and a description of them:

***1.The variables specified in gitlab itself are ci.yml:***

1. PROJECT_NAME - as the name suggests, this is the name of the project;
2. CONTAINER_TAG - methods for tagging docker images using the last commit characters;
3. URL_PROJECT - depending on the branch, either the real url of the site is used(whatweather.ru ), or from dev staging(dev.whatweather.ru )
4. ENV - the variable that displays the selected environment, depends on the branch;
5. APP_NAME - the name of the application, for example frontend, backend, db;
6. PATH_HELM - used to specify the path to the helm chart;
7. IMAGE_APP - used to specify the path in docker to the application image.

***2.Variables specified in gitlab ci (secrets) settings:***

1. DB_NAME_DEV/PROD - database name in base64 format;
2. DB_PASSWORD_DEV/PROD - password for accessing the database in base64 format;
3. DB_USERNAME_DEV/PROD - the user name for accessing the database in base64 format;
4. DB_URL_DEV/PROD - network path to the database(dns name);
5. DOCKER_HUB - url path to AWS ER; 
6. DOCKER_LOGIN - username for accessing AWS ECR;
7. DOCKER_PASS - token for access to AWS ECR;
8. K8S_CERTIFICATE_AUTHORITY_DATA - certificate for access to the kubernetes cluster;
9. K8S_CLUSTER_URL - url to the AWS EKS cluster;
10. K8S_TOKEN_CI - token for the gitlab user;
11. SONAR_HOST_URL - url for accessing sonarcloud;
12. SONAR_TOKEN - token for accessing sonarcloud;
13. URL_PROJECT_DEV - url for dev staging.
