import calendar
import json
import os
import time
from datetime import date, timedelta, datetime
from random import randrange, uniform, choices, randint
from operator import itemgetter

import mariadb
import requests
from flask import Flask
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from prometheus_flask_exporter import PrometheusMetrics

db_cred = {
    "user": os.getenv("username_db"),
    "password": os.getenv("password_db"),
    "host": os.getenv("name_service_db"),
    "database": os.getenv("database"),
}

app = Flask(__name__)
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = f"mysql+mysqldb://{db_cred['user']}:{db_cred['password']}@{db_cred['host']}:3306/{db_cred['database']}"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)
migrate = Migrate(app, db)

metrics = PrometheusMetrics(app, path=None)
metrics.start_http_server(8000)

list_current_date = list(map(int, str(date.today()).split("-")))
count_days_month = calendar.monthrange(list_current_date[0], list_current_date[1])[1]
today = list_current_date[2]

if int(today) + 5 > int(count_days_month):
    ends_range = count_days_month + 1
else:
    ends_range = int(today) + 5

names_table = [
    f"{list_current_date[0]}_{list_current_date[1]}_{day}"
    for day in range(1, ends_range)
]


def CreateTable(tablename):
    class date_tables(db.Model):
        __tablename__ = tablename
        id = db.Column(db.String(20), primary_key=True)
        weather_state_name = db.Column(db.String(20), nullable=True)
        wind_direction_compass = db.Column(db.String(20), nullable=True)
        created = db.Column(db.String(40), nullable=True)
        applicable_date = db.Column(db.DateTime(20), nullable=False)
        min_temp = db.Column(db.String(20), nullable=True)
        max_temp = db.Column(db.String(20), nullable=True)
        the_temp = db.Column(db.String(20), nullable=True)


def InsertToTable(name, req_sorted_by_created, cur, conn):
    for data_line in req_sorted_by_created:
        try:
            cur.execute(
                f"""INSERT INTO {name} (id, weather_state_name,
                            wind_direction_compass, created, applicable_date, min_temp,
                            max_temp, the_temp) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)""",
                (
                    data_line["id"],
                    data_line["weather_state_name"],
                    data_line["wind_direction_compass"],
                    data_line["created"],
                    data_line["applicable_date"],
                    round(data_line["min_temp"], 2),
                    round(data_line["max_temp"], 2),
                    round(data_line["the_temp"], 2),
                ),
            )
            conn.commit()
        except mariadb.Error:
            continue


def sorted_ends(s):
    res_find = s.find(".", 0)
    res = int(s[res_find + 1:])
    return res


def random_data_for_date(select_day):
    data_days = []
    this_day = select_day
    # все возможные вариации направления ветра
    wind_direction_compass = (
        "N",
        "NNE",
        "NE",
        "ENE",
        "E",
        "ESE",
        "SE",
        "SSE",
        "S",
        "SSW",
        "SW",
        "WSW",
        "W",
        "WNW",
        "NW",
        "NNW",
    )

    # возможные вариации погоды
    weather_state_name = (
        "Sleet",
        "Hail",
        "Thunderstorm",
        "Heavy Rain",
        "Light Rain",
        "Showers",
        "Heavy Cloud",
        "Light Cloud",
        "Clear",
    )

    # готовые списки после рандома
    wind = tuple(choices(wind_direction_compass, k=50))
    weather = tuple(choices(weather_state_name, k=50))

    for i in range(0, 50):
        ids = randrange(4000000000000000, 5000000000000000)
        min_temp = uniform(10.0, 15.0)
        max_temp = uniform(15.0, 20.0)
        the_temp = uniform(min_temp, max_temp)
        # created =
        st_day = int(this_day.split("_")[2])

        if st_day - 3 > 0:
            en_day = st_day - 3
            date = datetime(
                list_current_date[0],
                list_current_date[1],
                randint(en_day, st_day),
                randint(0, 23),
                randint(0, 59),
                randint(0, 59),
            )
        else:
            en_day = calendar.monthrange(
                list_current_date[0], list_current_date[1] - 1
            )[1]
            date = datetime(
                list_current_date[0],
                list_current_date[1] - 1,
                en_day,
                randint(0, 23),
                randint(0, 59),
                randint(0, 59),
            )
        # готовый словарь на одну запись
        res = {
            "id": ids,
            "weather_state_name": weather[i],
            "wind_direction_compass": wind[i],
            "created": date,
            "applicable_date": this_day,
            "min_temp": min_temp,
            "max_temp": max_temp,
            "the_temp": the_temp,
        }
        data_days.append(res)
    return data_days


def class_db(db, names_table):
    conn = mariadb.connect(**db_cred)
    cur = conn.cursor()
    for name in names_table:
        cur.execute(f"SHOW TABLES LIKE '{name}'")
        try:
            if name not in cur.fetchall()[0]:
                CreateTable(name)
            else:
                continue
        except IndexError:
            CreateTable(name)
    db.create_all()
    cur.close()
    conn.close()


def update_database(names_table):
    conn = mariadb.connect(**db_cred)
    cur = conn.cursor()
    # проверка на наличие старых таблиц и удаление их
    cur.execute("SHOW TABLES")
    table_in_database = {table[0]: 0 for table in cur.fetchall()}
    for table_key in table_in_database.keys():
        if table_key in names_table:
            table_in_database[table_key] = 1
        else:
            cur.execute(f"DROP TABLE {table_key}")
    # проверка на доступность сайта metaweather.com,
    # если недоступен включается рандомизатор данных
    if str(requests.get("https://www.metaweather.com/")) == "<Response [404]>":
        for name in names_table:
            req = random_data_for_date(name)
            req_sorted_by_created = sorted(req, key=itemgetter("created"))
            InsertToTable(name, req_sorted_by_created, cur, conn)
    else:
        for name in names_table:
            name_for_get = name.replace("_", "/")
            req = (
                requests.get(
                    f"https://www.metaweather.com/api/location/2122265/{name_for_get}"
                )
            ).json()
            req_sorted_by_created = sorted(req, key=itemgetter("created"))
            InsertToTable(name, req_sorted_by_created, cur, conn)
    cur.close()
    conn.close()


def delete_all_data():
    conn = mariadb.connect(**db_cred)
    cur = conn.cursor()
    cur.execute("SHOW TABLES")
    tables = cur.fetchall()
    for table in tables:
        print(table[0])
        cur.execute(f"TRUNCATE {table[0]}")
    cur.close()
    conn.close()


class_db(db, names_table)
update_database(names_table)


@app.route("/api/name_tables")
def table_dates():
    conn = mariadb.connect(**db_cred)
    cur = conn.cursor()
    cur.execute("SHOW TABLES")
    result_tables = sorted([date[0] for date in cur.fetchall()], key=sorted_ends)
    json_res = json.dumps(result_tables)
    cur.close()
    conn.close()
    return json_res


@app.route("/api/<date>")
def table_of_date(date):
    conn = mariadb.connect(**db_cred)
    cur = conn.cursor()
    cur.execute(f"SELECT * FROM {date}")
    result = json.dumps(cur.fetchall(), indent=4, sort_keys=True, default=str)
    cur.close()
    conn.close()
    return result


@app.route("/api/update")
def update():
    start_time = time.monotonic()
    class_db(db, names_table)
    update_database(names_table)
    end_time = time.monotonic()
    count_time = timedelta(seconds=end_time - start_time)
    json_res = json.dumps(count_time, indent=4, sort_keys=True, default=str)
    return json_res


@app.route("/api/delete")
def delete_data():
    start_time = time.monotonic()
    delete_all_data()
    end_time = time.monotonic()
    count_time = timedelta(seconds=end_time - start_time)
    json_res = json.dumps(count_time, indent=4, sort_keys=True, default=str)
    return json_res


@app.route("/healthz")
def healthz():
    return "OK"


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=3000)
