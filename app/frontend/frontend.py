import os

import requests
from flask import Flask, render_template, send_from_directory
from prometheus_flask_exporter import PrometheusMetrics

address_back = os.getenv("backend") + ":3000"


def format_date(date):
    date_form = date.replace("_", ".").split(".")
    date_form = ["0" + i if len(i) < 2 else i for i in date_form]
    date_form.reverse()
    date_f = ".".join(date_form)
    return date_f


app = Flask(__name__)
metrics = PrometheusMetrics(app, path=None)
metrics.start_http_server(8000)
app.config["TRAP_HTTP_EXCEPTIONS"] = True


@app.route("/", methods=["GET"])
def dates():
    name_table = requests.get(f"http://{address_back}/api/name_tables").json()
    dates_for_render = [(i, format_date(i)) for i in name_table]
    return render_template("date.html", result_tables=dates_for_render)


@app.route("/date/<date>", methods=["GET"])
def table_date(date):
    date_visible = format_date(date)
    table = requests.get(f"http://{address_back}/api/{date}").json()
    return render_template("table_date.html", result=table, date_visible=date_visible)


@app.route("/update", methods=["GET"])
def update():
    time = requests.get(f"http://{address_back}/api/update").json()
    return render_template("update.html", time=time)


@app.route("/delete", methods=["GET"])
def delete_data():
    time = requests.get(f"http://{address_back}/api/delete").json()
    return render_template("delete.html", time=time)


@app.route("/favicon.ico")
def favicon():
    return send_from_directory(
        os.path.join(app.root_path, "static"),
        "favicon.ico",
        mimetype="image/vnd.microsoft.icon",
    )


@app.errorhandler(404)
def page_not_found(error):
    return render_template("404.html"), 404


@app.errorhandler(500)
def internal_error(error):
    return render_template("500.html"), 500


@app.route("/healthz")
def healthz():
    return "OK"


if __name__ == "__main__":
    app.run()
