#!/bin/bash

# Заполнение данных заранее
DATABASE_NAME_PROD="flaskdbprod"
USERNAME_FOR_DB_PROD="flaskuserprod"
PASSWORD_FOR_DB_PROD="flaskpassprod"

DATABASE_NAME_DEV="flaskdbdev"
USERNAME_FOR_DB_DEV="flaskuserdev"
PASSWORD_FOR_DB_DEV="flaskpassdev"

# Заполнение ручным вводом в консоли
# echo -n "Please enter the database name for the production: "
# read DATABASE_NAME_PROD
# echo -n "Please enter the username of the database for the production: "
# read USERNAME_FOR_DB_PROD
# echo -n "Please enter the password of the database user for the production: "
# read PASSWORD_FOR_DB_PROD

# echo -n "Please enter the database name for the development: "
# read DATABASE_NAME_DEV
# echo -n "Please enter the username of the database for the development: "
# read USERNAME_FOR_DB_DEV
# echo -n "Please enter the password of the database user for the development: "
# read PASSWORD_FOR_DB_DEV

# base64 для переменных GITLAB CI VARIABLES
DB_NAME_PROD=$(echo -n $DATABASE_NAME_PROD | base64)
DB_USERNAME_PROD=$(echo -n $USERNAME_FOR_DB_PROD | base64)
DB_PASSWORD_PROD=$(echo -n $PASSWORD_FOR_DB_PROD | base64)

DB_NAME_DEV=$(echo -n $DATABASE_NAME_DEV | base64)
DB_USERNAME_DEV=$(echo -n $USERNAME_FOR_DB_DEV | base64)
DB_PASSWORD_DEV=$(echo -n $PASSWORD_FOR_DB_DEV | base64)


# Вывод переменных для TERRAFORM APPLY И GITLAB CI VARIABLES
echo -e "\033[0;32m\nTerraform variables [PROD]: "
echo -e "DATABASE NAME PROD = $DATABASE_NAME_PROD"
echo -e "USERNAME FOR DB PROD = $USERNAME_FOR_DB_PROD"
echo -e "PASSWORD FOR DB PROD = $PASSWORD_FOR_DB_PROD \n"

echo -e "\033[34m\nGITLAB CI variables [PROD]: "
echo -e "DB_NAME_PROD = $DB_NAME_PROD"
echo -e "DB_USERNAME_PROD = $DB_USERNAME_PROD"
echo -e "DB_PASSWORD_PROD = $DB_PASSWORD_PROD \n"


echo -e "\033[0;32m\nTerraform variables [DEV]: "
echo -e "DATABASE NAME DEV = $DATABASE_NAME_DEV"
echo -e "USERNAME FOR DB DEV = $USERNAME_FOR_DB_DEV"
echo -e "PASSWORD FOR DB DEV = $PASSWORD_FOR_DB_DEV \n"

echo -e "\033[34m\nGITLAB CI variables [DEV]: "
echo -e "DB_NAME_DEV = $DB_NAME_DEV"
echo -e "DB_USERNAME_DEV = $DB_USERNAME_DEV"
echo -e "DB_PASSWORD_DEV = $DB_PASSWORD_DEV \n"

read -p "Press any key to continue terraform apply infrastructure.. " -n1 -s

cd ./infrastructure/terraform/
terraform apply \
    -var "db_name_prod=${DATABASE_NAME_PROD}" \
    -var "db_username_prod=${USERNAME_FOR_DB_PROD}" \
    -var "db_password_prod=${PASSWORD_FOR_DB_PROD}" \
    -var "db_name_dev=${DATABASE_NAME_DEV}" \
    -var "db_username_dev=${USERNAME_FOR_DB_DEV}" \
    -var "db_password_dev=${PASSWORD_FOR_DB_DEV}" \
    -auto-approve