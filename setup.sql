-- create the databases
CREATE DATABASE IF NOT EXISTS test_flask;

-- create the users for each database
CREATE USER 'testuser'@'%' IDENTIFIED BY 'testpass';
GRANT CREATE, ALTER, INDEX, LOCK TABLES, REFERENCES, UPDATE, DELETE, DROP, SELECT, INSERT ON `test_flask`.* TO 'testuser'@'%';

FLUSH PRIVILEGES;