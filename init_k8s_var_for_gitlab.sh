#!/bin/bash

cluster_name=whatweather-cluster
region=eu-central-1
# Получаем конфигурационные данные для подключения к AWS EKS
aws eks update-kubeconfig --region $region --name $cluster_name

# Настраиваем пользователя для gitlab ci
kubectl apply -f infrastructure/k8s/gitlab-account.yaml

tk_name_t=$(kubectl describe serviceaccount/gitlab-service-account | grep Tokens: | cut -d ":" -f 2)
# Получаем имя секрета
tk_name=${tk_name_t// /}

token_t=$(kubectl describe secret $tk_name | grep token: | cut -d ":" -f 2)
# Получаем токен для переменной $K8S_TOKEN_CI
token_ci=${token_t// /}

# Получаем адрес кластера для переменной $K8S_CLUSTER_URL
kubernetes_url=$(kubectl cluster-info | grep "Kubernetes control plane" | cut -d " " -f 7)

# Получаем сертификат для переменной $K8S_CERTIFICATE_AUTHORITY_DATA
kubernetes_cert=$(cat ~/.kube/config | grep "$region.eks.amazonaws.com" -B 1 | grep certificate-authority-data | cut -d " " -f 6)

# Получаем токен для логина в AWS ECR, для переменной $DOCKER_PASS
docker_pass=$(aws ecr get-login-password --region $region)

# Получаем url AWS ECR, для переменной $DOCKER_HUB
docker_url=$(aws ecr describe-registry | grep registry | cut -d ":" -f 2 | sed 's/"//g' | sed 's/,//g')

# Вывод переменных
echo -e "\033[0;32m\nK8S_TOKEN_CI = \033[34m${token_ci} \n"
echo -e "\033[0;32mK8S_CERTIFICATE_AUTHORITY_DATA = \033[34m${kubernetes_cert} \n"
echo -e "\033[0;32mK8S_CLUSTER_URL = \033[34m${kubernetes_url} \n"
echo -e "\033[0;32mDOCKER_HUB = \033[34m${docker_url}.dkr.ecr.eu-central-1.amazonaws.com \n"
echo -e "\033[0;32mDOCKER_PASS = \033[34m${docker_pass} \n"